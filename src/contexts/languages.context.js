import React from 'react';
export const Languages = {
	ES: {
		name: 'ES',
		job: 'Desarrollador Web con Reactjs y Django.',
		title: 'Hola Mundo!',
		biography:
			'Soy Leonardo Nava, estudiaré física, me gustaría ser un científico de datos. Me gusta aprender cosas nuevas todos los días, así es como aprendí a programar, aprendiendo solo, después de unos meses descubrí platzi donde realmente fue un cambio en mi aprendizaje. Soy alguien a quien le gusta leer mucho, leo fantasía épica, matemáticas, física, hago mi mejor esfuerzo para mejorar día a día.',
		buttons: ['Certificados', 'Proyectos']
	},
	US: {
		name: 'US',
		job: 'Web app developer with ReactJs and Django.',
		title: 'Hello, World',
		biography:
			'I am Leonardo Nava, I will study physics, I would like to be a data scientist. I like learning new things every day, this is how I learned to program, learning alone, after a few months I discovered platzi where it really was a change in my learning. I am someone who likes to read a lot, I read epic fantasy, math, physics, I do my best to improve day by day.',
		buttons: ['Certificates', 'Projects']
	}
};

export const LanguageContext = React.createContext(Languages['US']);
export const Provider = LanguageContext.Provider;
export const Consumer = LanguageContext.Consumer;

